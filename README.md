# tic-tac-toe-challenge

[Python] : Tic Tac Toe Challenge

### Objectives

Write a simple program which pretends to play tic-tac-toe with the user.
To make it all easier for you, we've decided to simplify the game. 

Here are our assumptions:

- The computer (i.e., your program) should play the game using 'X's
- The user (e.g., you) should play the game usin  g 'O's
- The first move belongs to the computer - it always puts its first 'X' in the middle of the board
- All the squares are numbered row by row starting with 1 (see the example session below for reference)
- The user inputs their move by entering the number of the square they choose - the number must be valid, i.e., it must be an integer, it must be greater than 0 and less than 10, and it cannot point to a field which is already occupied
- The program checks if the game is over - there are four possible verdicts: the game should continue, or the game ends with a tie, your win, or the computer's win
- The computer responds with its move and the check is repeated
- Don't implement any form of artificial intelligence - a random field choice made by the computer is good enough for the game


### Requirements
Implement the following features:

- The board should be stored as a three-element list, while each element is another three-element list (the inner lists represent rows) so that all of the squares may be accessed using the following syntax:
board[row][column]

- Each of the inner list's elements can contain 'O', 'X', or a digit representing the square's number (such a square is considered free)

- The board's appearance should be exactly the same as the one presented in the bellow  example 

<img src='challenge_coding_python.png'>